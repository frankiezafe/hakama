# HAKAMA

blueprint for an hakama trouser, based on a jpg found online

## mounting

![overview](https://gitlab.com/frankiezafe/hakama/-/raw/master/imgs/assembly_overview.png?ref_type=heads)

## log

*2024.04.18*

correction on the design after prototyping with calico. 

- internal part of the pocket used as the joint to open the trouser;
- blueprint is simplified and belts are reinforced by making the main pieces longer;
- the length of the legs has no been validated yet.

*2023.12.16*

corrected version with reviewed koshita and large inner pocket

*2023.11.18*

currently in prototype phase, first paper mounting successfull even if a lot of imperfection are present
